﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSqlClient
{
    public interface IProfessorRepository:IRepository<Professor>
    {
        public List<Professor> GetAll();
        public bool Add(Professor newProfessor);

        public bool DeleteProfessor(Professor newProfessor);
        public bool UpdateProfessor(Professor newProfessor);
    }
}
