# SqlClient Demo Code
This is demo code from module 2 - lesson 2.3.
Don't forget to update your connection string. It won't working if you're using my default connection string.
Also make sure that you're accessing an appropriate SQL database with a table named "Professor".  

## Installation
To install, clone with git
```
git clone https://gitlab.com/teacher.warren.english/demo-sql-client.git
```

## Contributing
Warren West
