﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DemoSqlClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IProfessorRepository repo = new ProfessorRepository();

            Professor fred = new Professor()
            {
                FirstName = "Fred",
                LastName = "Flintstone",
            };

            if (repo.AddProfessor(fred))
            {
                Console.WriteLine($"{fred.FirstName} was successfully added to the DB!");
            }

            List<Professor> myProfs = repo.GetAllProfessors();

            foreach (var p in myProfs)
            {
                Console.WriteLine($"{p.Id}\t{p.FirstName}\t{p.LastName}");
            }
        }
    }
}
