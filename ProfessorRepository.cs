﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSqlClient
{
    public class ProfessorRepository : IProfessorRepository
    {
        public bool AddProfessor(Professor newProfessor)
        {
            try
            {
                using SqlConnection connection = new SqlConnection(UniversityDbConfig.GetConnectionString());
                connection.Open();

                string sql = "INSERT INTO Professor (FirstName, LastName) VALUES (@firstname, @lastname)";

                SqlCommand cmd = new SqlCommand(sql, connection);

                cmd.Parameters.AddWithValue("@firstname", newProfessor.FirstName);
                cmd.Parameters.AddWithValue("@lastname", newProfessor.LastName);

                return cmd.ExecuteNonQuery() == 1;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return false;
        }

        public bool DeleteProfessor(Professor newProfessor)
        {
            throw new NotImplementedException();
        }

        public List<Professor> GetAllProfessors()
        {
            List<Professor> professors = new List<Professor>();

            try
            {
                using SqlConnection connection = new SqlConnection(UniversityDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand("SELECT * FROM Professor", connection);
                using SqlDataReader myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    Professor professor = new Professor()
                    {
                        Id = myReader.GetInt32(0),
                        FirstName = myReader.GetString(1),
                        LastName = myReader.GetString(2),
                    };
                    professors.Add(professor);
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (InvalidCastException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return professors;
        }

        public bool UpdateProfessor(Professor newProfessor)
        {
            throw new NotImplementedException();
        }
    }
}
