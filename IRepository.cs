﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSqlClient
{
    public interface IRepository<T> // not necessary for Assignment 2
    {
        public bool Add(T newEntity);
        public List<T> GetAll();
        // update()
        // delete()
        // getById()
    }
}
